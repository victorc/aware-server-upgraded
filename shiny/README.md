# Shiny Dashboard

## Initial Setup
#### (AS OF MAY 14, 2018) Prior to Installing the Dashboard, please make sure you have the latest version of R (version 3.4.4 - Someone to Lean On) that is supported by the application. It is available on the following site: https://www.r-project.org/

#### Please make sure you have the following files prior to setup (most found in the Shiny directory):
- ui.r
- server.r
- graphs.r
- icons.r
- modals.r
- sql.r
- config.yml - includes credentials for the database as well as descriptions of the study the dashboard is being set up for
  - The file should be in the following format:
    ```
    db:
      host : HOST_NAME
      name : DATABASE_NAME
      user : USER_NAME
      password : PASSWORD
      port : PORT_NUMBER
    ```
#### Add the files to a directory in the shiny-server directory on the r2d2 server
#### The dashboard should now be accessible at r2d2.hcii.cs.cmu.edu:8080/$DIRECTORY_NAME$

## Configurations on Dashboard
#### Use the Dashboard or the Database Server to check if the Last Received Data table exists. If not, go to the Configurations tab and select "Create Table" to create the table in the database. The dashboard should refresh and the changes should be made.
#### With the Last Received Data table, the Configurations tab should be different and should include which sensors to display/hide, which sensors to include triggers for, and a button to update the Last Received Data table. 
#### The triggers for the sensors which we want to monitor should be enabled so that the Last Received Data table is automatically updated for these sensors when new data comes in. 
#### If there is already pre-existing data, you select "Update Table" so that the Last Received Data table can be updated with the most recent dates for each sensor and device IDs.

## Add-ons to Dashboard (Not Implemented in Code)
#### You can monitor the number of records for each sensor by adding a `_SENSOR_day` table to the database (for each SENSOR). The table must have the following fields and corresponding types: `device_id` (VARCHAR), `num_records` (INT), `datetime_EST` (DATETIME). Then, for each of the sensors with such tables, add the following trigger into the Last Received Data table (`_last_received_data` on most dashboards) in the database. Note that the sensors themselves must be a field within the Last Received Data table in order for the trigger to work.
  ```
  BEGIN
    IF (NEW.SENSOR1 <> OLD.SENSOR1) THEN
      INSERT INTO `_SENSOR1_day` (`device_id`, `datetime_EST`, `num_records`) VALUES (`device_id`, DATE(NEW.SENSOR1), 1)
      ON DUPLICATE KEY UPDATE `num_records` = `num_records` + 1;
    END IF;
    IF (NEW.SENSOR2 <> OLD.SENSOR2) THEN
      INSERT INTO `_SENSOR2_day` (`device_id`, `datetime_EST`, `num_records`) VALUES (`device_id`, DATE(NEW.SENSOR2), 1)
      ON DUPLICATE KEY UPDATE `num_records` = `num_records` + 1;
    END IF;
    IF (NEW.SENSOR3 <> OLD.SENSOR3) THEN
      INSERT INTO `_SENSOR3_day` (`device_id`, `datetime_EST`, `num_records`) VALUES (`device_id`, DATE(NEW.SENSOR3), 1)
      ON DUPLICATE KEY UPDATE `num_records` = `num_records` + 1;
    END IF;
  END
  ```
